const express = require('express')
const axios = require('axios')
const router = express.Router()
const User = require('../models/user')



// Get de todos los datos de la base de datos

router.get('/', async (req, res) => {
  try {
    const users = await User.find()
    res.json(users)
    res.render('createNew' , {title : 'first'})
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})

//Funcion para mostrar registros
function mirar(){
  axios.get('http://localhost:3000/users' , {
    responseType : 'json'
  })
  .then((response) => {
    console.log("\n\nRegistros actuales")
    response.data.forEach(element => 
      console.log(
        "------------------------\n"
        + "nombre : " + element["name"]+ "\n"
        + "symptoms : " + element["symptoms"]
        )
      );
  });
}


console.log(mirar())


// Get de un solo usuario
router.get('/:id', getUser, (req, res) => {
  res.json(res.user)
})

// Post de usuario
router.post('/', async (req, res) => {
  const user = new User({
    name: req.body.name,
    symptoms: req.body.symptoms
  })
  try {
    const newUser = await user.save()
    res.status(201).json(newUser)
  } catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Update de un usuario
router.patch('/:id', getUser, async (req, res) => {
  if (req.body.name != null) {
    res.user.name = req.body.name
  }
  if (req.body.symptoms != null) {
    res.user.symptoms = req.body.symptoms
  }
  try {
    const updatedUser = await res.user.save()
    res.json(updatedUser)
  } catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Eliminacion de documento
router.delete('/:id', getUser, async (req, res) => {
  try {
    await res.user.remove()
    res.json({ message: 'Deleted User' })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
})


async function getUser(req, res, next) {
  let subscriber
  try {
    user = await User.findById(req.params.id)
    if (user == null) {
      return res.status(404).json({ message: 'Cannot find subscriber' })
    }
  } catch (err) {
    return res.status(500).json({ message: err.message })
  }

  res.user = user
  next()
}

module.exports = router



