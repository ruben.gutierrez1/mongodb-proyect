/*
1 tipos de datos 

2 manejo de las colecciones

4 como hace las consultas

5 num domcumentos

6 operadores logicos

7 tiene modelo de datos coherente

9 manejo de indices

10 maneja replicas

11 es necesaria fragmentacion y las documenta

puntos del trabajo

*/ 

const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  symptoms: {
    type: String,
    required: true
  },
  subscribeDate: {
    type: Date,
    required: true,
    default: Date.now
  }
})

module.exports = mongoose.model('User', userSchema)