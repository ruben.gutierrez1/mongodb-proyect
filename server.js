require('dotenv').config()

const express = require('express')
const app = express()
const mongoose = require('mongoose')
const path = require('path')

//Opciones para control de vistas
/*
app.use(express.static(__dirname + "/public"))
*/

app.set('views', path.join(__dirname , 'public'))
app.set('view engine' , 'ejs')

app.get('/', (req ,res) => {
    res.render('index')
})


//Conexion con el servidor
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true , useUnifiedTopology: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database'))

app.use(express.json())


//Db que vamos a utilzar
const usersRouter = require('./routes/users')
app.use('/users', usersRouter)

//Configuracion del puerto

app.listen(3000, () => console.log('Server Started'))